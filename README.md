# SimpleFormExtensions

SimpleFormExtensions are SimpleForm and Twitter Bootstrap integration.
This allow you to use input with append o prepend span.

## Installation

Add the gem to your project's `Gemfile` as follows:

```ruby
gem 'simple_form_extensions'
```

Then install it by running:

```bash
bundle install
```

## Getting Started


### Input checkbox

For `inline_checkbox`:

```
<%= simple_form_for @user, :html => { :class => 'form-horizontal' } do |f| %>
  <%= f.input :remember_me, wrapper: :inline_checkbox %>
<% end %>
```

This generate the following markup:

```
<form class="simple_form form-horizontal">
  <div class="control-group">
    <div class="controls">
      <div class="pull-left">
        <input name="user[remember_me]" type="hidden" value="0">
        <label class="checkbox">
          <input id="user_remember_me" name="user[remember_me]" type="checkbox" value="1">
        </label>
      </div>
      Remember me
    </div>
  </div>
</form>
```

And the result:

![Inline checkbox 1](https://bitbucket.org/mespina/simple_form_extensions/raw/master/app/assets/images/simple_form_extensions/inline_checkbox.png)


### Appending

For `append` value into input:

```
<%= simple_form_for @user, :html => { :class => 'form-horizontal' } do |f| %>
  <%= f.input :percentage, wrapper: :e_append %>
<% end %>
```

This generate the following markup:

```
<form class="simple_form form-horizontal">
  <div class="control-group">
    <label class="control-label" for="user_percentage">Percentage</label>
      <div class="controls">
        <div class="input-append">
          <input name="user[percentage]" type="text">
          <span class="add-on">%</span>
        </div>
      </div>
    </div>
  </div>
</form>
```

And the result:

![Append 1](https://bitbucket.org/mespina/simple_form_extensions/raw/master/app/assets/images/simple_form_extensions/append_1.png)


### Prepending

For `prepend` value into input:

```
<%= simple_form_for @user, :html => { :class => 'form-horizontal' } do |f| %>
  <%= f.input :price, wrapper: :e_prepend %>
<% end %>
```

This generate the following markup:

```
<form class="simple_form form-horizontal">
  <div class="control-group">
    <label class="control-label" for="user_price">Price</label>
      <div class="controls">
        <div class="input-prepend">
          <span class="add-on">$</span>
          <input name="user[price]" type="text">
        </div>
      </div>
    </div>
  </div>
</form>
```

And the result:

![Prepend 1](https://bitbucket.org/mespina/simple_form_extensions/raw/master/app/assets/images/simple_form_extensions/prepend_1.png)


### Prepending and Appending

For `append` and `prepend` values into input:

```
<%= simple_form_for @user, :html => { :class => 'form-horizontal' } do |f| %>
  <%= f.input :sample, wrapper: :e_prepend_append %>
<% end %>
```

This generate the following markup:

```
<form class="simple_form form-horizontal">
  <div class="control-group">
    <label class="control-label" for="user_sample">Sample</label>
      <div class="controls">
        <div class="input-prepend input-append">
          <span class="add-on">$</span>
          <input name="user[sample]" type="text">
          <span class="add-on">%</span>
        </div>
      </div>
    </div>
  </div>
</form>
```

And the result:

![Prepending and Appending 1](https://bitbucket.org/mespina/simple_form_extensions/raw/master/app/assets/images/simple_form_extensions/prepend_append_1.png)


### Customize

By default `append` and `prepend` have default values:


 Value   | Default
:--------|:-------:
 append  | %
 prepend | $


You can change de default values, creating your locale:

```YML
en:
  simple_form_extensions:
    default_prepend: YOUR_CUSTOM_DEFAULT_FOR_PREPEND_HERE
    default_append:  YOUR_CUSTOM_DEFAULT_FOR_APPEND_HERE
```

Or you can change only one:

```
<%= simple_form_for @user, :html => { :class => 'form-horizontal' } do |f| %>
  <%= f.input :sample, wrapper: :e_prepend_append, prepend: '€', append: '.00' %>
<% end %>
```

This generate the following markup:

```
<form class="simple_form form-horizontal">
  <div class="control-group">
    <label class="control-label" for="user_sample">Sample</label>
      <div class="controls">
        <div class="input-prepend input-append">
          <span class="add-on">€</span>
          <input name="user[sample]" type="text">
          <span class="add-on">.00</span>
        </div>
      </div>
    </div>
  </div>
</form>
```

And the result:

![Prepending and Appending 2](https://bitbucket.org/mespina/simple_form_extensions/raw/master/app/assets/images/simple_form_extensions/prepend_append_2.png)


Or you can try using icons:

```
<%= simple_form_for @user, :html => { :class => 'form-horizontal' } do |f| %>
  <%= f.input :sample, prepend: "<i class='icon-plus'></i>", append: "<i class='icon-minus'></i>"  %>
<% end %>
```

This generate the following markup:

```
<form class="simple_form form-horizontal">
  <div class="control-group">
    <label class="control-label" for="user_sample">Sample</label>
      <div class="controls">
        <div class="input-prepend input-append">
          <span class="add-on">
            <i class="icon-plus"></i>
          </span>
          <input name="user[sample]" type="text">
          <span class="add-on">
            <i class="icon-minus"></i>
          </span>
        </div>
      </div>
    </div>
  </div>
</form>
```

And the result:

![Prepending and Appending 3](https://bitbucket.org/mespina/simple_form_extensions/raw/master/app/assets/images/simple_form_extensions/prepend_append_3.png)
