$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "simple_form_extensions/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "simple_form_extensions"
  s.version     = SimpleFormExtensions::VERSION
  s.authors     = ["mespina"]
  s.email       = ["mespina.icc@gmail.com", "cristhian.pires@gmail.com"]
  s.summary     = "SimpleForm extension for Bootstrap."
  s.description = "SimpleForm extension for Bootstrap append & prepend markup. "
  s.homepage    = "https://bitbucket.org/territorial/simple_form_extensions"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails"#, "~> 4.0.1"
  # s.add_dependency "jquery-rails"

end
