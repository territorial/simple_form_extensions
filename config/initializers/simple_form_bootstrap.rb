# Use this setup block to configure all options available in SimpleForm.
SimpleForm.setup do |config|
  config.wrappers :e_prepend, :tag => 'div', :class => 'control-group', :error_class => 'error' do |b|
    b.use :html5
    b.use :placeholder
    b.use :label
    #b.wrapper :tag => 'div', :class => 'controls' do |input|
    b.wrapper :tag => 'div', :class => '' do |input|
      input.wrapper :tag => 'div', :class => 'input-prepend' do |prepend|
        prepend.use :prepend, :wrap_with => { :tag => 'span', :class => 'add-on'}
        prepend.use :input
      end
      input.use :hint,  :wrap_with => { :tag => 'span', :class => 'help-block' }
      input.use :error, :wrap_with => { :tag => 'span', :class => 'help-inline' }
    end
  end

  config.wrappers :e_append, :tag => 'div', :class => 'control-group', :error_class => 'error' do |b|
    b.use :html5
    b.use :placeholder
    b.use :label
    #b.wrapper :tag => 'div', :class => 'controls' do |input|
    b.wrapper :tag => 'div', :class => '' do |input|
      input.wrapper :tag => 'div', :class => 'input-append' do |prepend|
        prepend.use :input
        prepend.use :append, :wrap_with => { :tag => 'span', :class => 'add-on'}
      end
      input.use :hint,  :wrap_with => { :tag => 'span', :class => 'help-block' }
      input.use :error, :wrap_with => { :tag => 'span', :class => 'help-inline' }
    end
  end

  config.wrappers :e_prepend_append, :tag => 'div', :class => 'control-group', :error_class => 'error' do |b|
    b.use :html5
    b.use :placeholder
    b.use :label
    b.wrapper :tag => 'div', :class => 'controls' do |input|
      input.wrapper :tag => 'div', :class => 'input-prepend input-append' do |prepend|
        prepend.use :prepend, :wrap_with => { :tag => 'span', :class => 'add-on'}
        prepend.use :input
        prepend.use :append, :wrap_with => { :tag => 'span', :class => 'add-on'}
      end
      input.use :hint,  :wrap_with => { :tag => 'span', :class => 'help-block' }
      input.use :error, :wrap_with => { :tag => 'span', :class => 'help-inline' }
    end
  end

  config.wrappers :inline_checkbox, :tag => 'div', :class => 'control-group', :error_class => 'error' do |b|
    b.use :html5
    b.use :placeholder
    b.wrapper :tag => 'div', :class => 'controls' do |ba|
      ba.use :input, class: 'pull-left'
      ba.use :label_text

      ba.use :error, :wrap_with => { :tag => 'span', :class => 'help-inline' }
      ba.use :hint,  :wrap_with => { :tag => 'p', :class => 'help-block' }
    end
  end
end
