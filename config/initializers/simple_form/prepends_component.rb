module SimpleForm
  module Components
    # Needs to be enabled in order to do automatic lookups
    module Prepends
      # Name of the component method
      def prepend(wrapper_options)
        @prepend ||= begin
          default = I18n.t('simple_form_extensions.default_prepend').include?('translation missing:') ? '$' : I18n.t('simple_form_extensions.default_prepend')
          options[:prepend].present? ? options[:prepend].to_s.html_safe : default
        end
      end

      # Used when the prepend is optional
      def has_prepend?
        prepend.present?
      end
    end
  end
end

SimpleForm::Inputs::Base.send(:include, SimpleForm::Components::Prepends)