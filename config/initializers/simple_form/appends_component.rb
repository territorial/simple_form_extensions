module SimpleForm
  module Components
    # Needs to be enabled in order to do automatic lookups
    module Appends
      # Name of the component method
      def append(wrapper_options)
        @append ||= begin
          default = I18n.t('simple_form_extensions.default_append').include?('translation missing:') ? '%' : I18n.t('simple_form_extensions.default_append')
          options[:append].present? ? options[:append].to_s.html_safe : default
        end
      end

      # Used when the append is optional
      def has_append?
        append.present?
      end
    end
  end
end

SimpleForm::Inputs::Base.send(:include, SimpleForm::Components::Appends)